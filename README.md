#Test driven development/Behavior driven development

One test, two different development approach

##Requirements
* [Nose tests](https://nose.readthedocs.org/en/latest/)
* [Behave](https://pythonhosted.org/behave/)
* pip install nose
* pip install behave

##Folders
* src
* tdd
* bdd