#!usr/bin/env python
# -*- coding: utf-8 -*-
"""
A simple string calculator
"""
from xdrlib import ConversionError


class Calculator(object):

    """
     A simple string calculator
     You can pass 0,1 or 2 numbers and will return the sum
     """

    def __init__(self):
        pass

    def add(self, entry=None):
        # return 0 if no numbers
        if not entry:
            return 0
        # check numbers
        if entry[0].isdigit():
            delimiter = '-*-any-*-'
            numbers = entry
        else:
            full_entry = entry.split('\n')
            begining = str(entry[0]) + str(entry[1])
            if begining == '//':
                delimiter = full_entry[0][2:]
                numbers = full_entry[1]
        # sum
        if delimiter == '-*-any-*-':
            (result, negatives) = self.sum_any(numbers)
        else:
            (result, negatives) = self.sum_delimeter(numbers, delimiter)
        # no negatives
        if len(negatives):
            neg_list = ','.join(negatives)
            raise ValueError('no negatives allowed %s' % str(neg_list))
        return result

    def sum_any(self, numbers):
        result = 0
        negatives = []
        pos = 0
        for char in numbers:
            if char.isdigit():
                number = int(char)
                if number < 1000:
                    result += number
            elif char == '-' and numbers[pos + 1].isdigit():
                number = '-%s' % numbers[pos + 1]
                negatives.append(str(number))
            pos += 1
        return (result, negatives)

    def sum_delimeter(self, numbers, delimiter):
        result = 0
        negatives = []
        numbers = numbers.split(delimiter)
        for char in numbers:
            number = int(char)
            if number < 1000:
                result += number
            if number < 0:
                negatives.append(str(number))
        return (result, negatives)

if __name__ == '__main__':
    calc = Calculator()
#     print calc.add('1,2')
#     print calc.add('1\n2,3')
#     print calc.add('//;\n1;2')
#     print calc.add('1,2,-3')
#     calc.add('//,\n1,2,-3')
    calc.add('1,2,-3')
