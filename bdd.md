#Behavior Driven Development (BDD): Value Through Collaboration

[Link](http://technologyconversations.com/2013/11/14/behavior-driven-development-bdd-value-through-collaboration-part-1-introduction/)

Behavior-Driven Development (BDD) is an agile process designed to keep the focus on stakeholder value throughout the whole project.
The premise of BDD is that the requirement has to be written in a way that everyone understands it – business representative, analyst, developer, tester, manager, etc.
The key is to have a unique set of artifacts that are understood and used by everyone.
A BDD story is written by the whole team and used as both requirements and executable test cases.
It is a way to perform test-driven development (TDD) with a clarity that can not be accomplished with unit testing.
It is a way to describe and test functionality in (almost) natural language.

##BDD Story Format

Even though there are different variations of the BDD story template, they all have two common elements: narrative and scenario. Each narrative is followed by one or more scenarios.

```
Narrative:  
In order to [benefit]  
As a [role]  
I want to [feature]  

Scenario: [description]  
Given [context or precondition]  
When [event or action]  
Then [outcome validation]  
```

## Narrative

A narrative is a short, simple description of a feature told from the perspective of a person or role that requires the new functionality.

The intention of the narrative is NOT to provide a complete description of what is to be developed but to provide a basis for communication between all interested parties

Even though it is usually very short, it tries to answer three basic questions that are often overlooked in traditional requirements.

* What is the benefit or value that should be produced (In order to)?
* Who needs it (As a)?
* What is a feature or goal (I want to)?

```
Narrative:
In order to preserve my work
As a blog writer
I want to be able to save posts
```

A good BDD narrative uses the “INVEST” model:

* Independent. Reduced dependencies = easier to plan.
* Negotiable. Details added via collaboration.
* Valuable. Provides value to the customer.
* Estimable. Too big or too vague = not estimable.
* Small. Can be done in less than a week by the team.
* Testable. Good acceptance criteria defined as scenarios.

##Scenarios

Scenarios describe interactions between user roles and the system. They are written in plain language with minimal technical details so that all stakeholders (customer, developers, testers, designers, marketing managers, etc) can have a common base for use in discussions, development, and testing.

Scenarios consist of a description and given, when, and then steps.

The scenario description is a short explanation of what the scenario does. It should be possible to understand the scenario from its description. It should not contain details and should not be longer than ten words.

The given step describes the context or precondition that needs to be fulfilled.

```
Given visitor is on the home screen
```

The when step describes an action or some event.

```
When user logs in
```

The then step describes an outcome.

```
Then welcome message is displayed
```

Any number of given, when and then steps can be combined, but at least one of each must be present. 

The following process should be followed.

1. Write and discuss narrative.
2. Write and discuss short descriptions of scenarios.
3. Write steps for each scenario.
4. Repeat steps 2 and 3 during the development of the narrative.

Following narrative shall be used as an example

```
Narrative:
In order to have a customized experience
As a site visitor
I want to be able to login
```

Once this feature has been discussed with representatives of the role (in this case a site visitor) the following initial conclusions can be drawn.

* Visitors should be able to register.
* Existing users should be able to log in.
* There should be an option to retrieve a password.

These conclusions should be written as scenario descriptions.

```
Scenario: Visitors are able to register

Scenario: Users are able to log in

Scenario: Users are able to retrieve their password
```

By starting only with scenario descriptions, we are creating a basis that will be further developed through steps. It allows us to discuss different aspects of the narrative without going into the details of all the steps required for each of the scenarios.

At this point we can start working on our first scenario. It could look as follows:

```
Scenario: Visitors are able to register

Given the visitor is on the home screen
When the visitor clicks the register button
Then login screen is loaded
When the visitor types his email
When the visitor types his password
When the visitor clicks the register button
Then confirmation screen is loaded
```

**vaidation table**

The examples table provides the means to describe different iterations of the same scenario by moving values that change from one iteration to another into a table.

```
Scenario: Visitors are able to register

Given the visitor is on the home screen
Given the visitor is NOT logged in
When the visitor clicks the register button
Then the login screen is loaded
When the visitor types the email <email>
When the visitor types the password <password>
When the visitor types the validation password <validationPassword>
When the visitor clicks the register button
Then the text <text> is displayed

Examples:
|email                |password |password2 |text                                       |
|john.doe@example.com |mysecret |mysecret  |Welcome john.doe@example.com               |
|john.doe@example.com |secret   |secret    |Password must be at least 8 characters long|
|john.doe@example.com |mysecret |something |Validation password is NOT the same        |
|john.doe             |mysecret |mysecret  |Email is incorrect                         |
```

##Automation

Successful implementation of the BDD automation should be done in three phases.

1. Create library of normalized steps.
2. Combine steps into composites.
3. Empower scenarios with examples tables.

You should start with the first phase. As soon as enough steps have been created to support the first scenario, you can start with execution of the other two phases. From there on, work on all three phases should be done in parallel.

**Phase #1: Create library of normalized steps**  

* Separate the scenarios from the code
* You could use BDD as a substitue of Unit Testing
* Build the steps library

```
CommonSteps - WebSteps        - MyWebSiteSteps
                              - BackOfficeSteps
            - DatabaseSteps   - OracleSteps
                              - MySQLSteps
                              - DB2Steps
            - APISteps        - JSONSteps
                              - WebServiceSteps
            - OperationsSteps - ShellSteps
                              - WindowsSteps
                              - TelnetSteps
```

* Create small atomic steps (Simple code is always better than complicated code.)

```
[BDD Story]
When the visitor types john.doe@example.com into the email field
When the visitor types mysecret into the password field
When the visitor clicks the register button
```

* Standardize steps format
* Externalize parameters
* Avoid repeated code with aliases

**Phase #2: Combine steps into composites**  

**Phase #3: Empower scenarios with examples tables**


