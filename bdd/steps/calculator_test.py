from behave import *
from src.calculator import Calculator

numbers_list = ''


@given('a list of numbers separated by commas')
def step_impl(context):
    global numbers_list
    numbers_list = '1,2,3'


@when('we send the list of numbers "1,2,3"')
def step_impl(context):
    global numbers_list
    assert numbers_list == '1,2,3'


@then('we have to get the sum of the numbers "6"')
def step_impl(context):
    global numbers_list
    calc = Calculator()
    result = calc.add(numbers_list)
    assert result == 6
