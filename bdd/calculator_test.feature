Feature: calculator demostration

	Scenario: run a simple string calculator test
		Given a list of numbers separated by commas
		when we send the list of numbers "1,2,3"
		then we have to get the sum of the numbers "6"