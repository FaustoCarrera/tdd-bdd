#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Calculator test
"""

from nose.tools import *
from src.calculator import Calculator


class TestCalculator(object):

    @classmethod
    def setup_class(self):
        self.calc = Calculator()

    def test_0_numbers(self):
        result = self.calc.add()
        assert_equal(result, 0)

    def test_1_numbers(self):
        # one number
        result = self.calc.add('1')
        assert_equal(result, 1)

    def test_2_numbers(self):
        # two numbers
        result = self.calc.add('1,2')
        assert_equal(result, 3)

    def test_3_numbers(self):
        # three numbers
        result = self.calc.add('1,2,3')
        assert_equal(result, 6)

    def test_4_delimiters(self):
        # three numbers
        result = self.calc.add('1\n2,3')
        assert_equal(result, 6)

    def test_5_delimiters(self):
        # three numbers
        result = self.calc.add('//;\n1;2;3')
        assert_equal(result, 6)

    @raises(ValueError)
    def test_6_negative(self):
        # three numbers
        self.calc.add('//;\n1;2;-3;-4'),

    @raises(ValueError)
    def test_7_negative(self):
        # three numbers
        self.calc.add('1,2,-3,-4'),

    def test_8_higher(self):
        # three numbers
        result = self.calc.add('//;\n2;1000')
        assert_equal(result, 2)

    def test_9_len(self):
        # three numbers
        result = self.calc.add('//--\n1--2--3')
        assert_equal(result, 6)
